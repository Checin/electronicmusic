﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicMusic
{
    public class AlbumEntry
    {
        public string Cover { get; set; }

        public string Name { get; set; }
    }
}
