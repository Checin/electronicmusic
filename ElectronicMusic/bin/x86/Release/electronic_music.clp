
;;;======================================================
;;;   Automotive Expert System
;;;
;;;     This expert system diagnoses some simple
;;;     problems with a car.
;;;
;;;     CLIPS Version 6.3 Example
;;;
;;;     For use with the Auto Demo Example
;;;======================================================

;;;*****************
;;;* Configuration *
;;;*****************
   
(defglobal ?*target* = gui) ; console, cgi, or gui

;;; ***************************
;;; * DEFTEMPLATES & DEFFACTS *
;;; ***************************

(deftemplate UI-state
   (slot id (default-dynamic (gensym*)))
   (multislot display)
   (slot relation-asserted (default none))
   (slot response (default none))
   (multislot valid-answers)
   (multislot display-answers)
   (slot state (default middle)))
   
(deftemplate state-list
   (slot current)
   (multislot sequence))
  
(deffacts startup
   (state-list))
;;;***************************
;;;* DEFFUNCTION DEFINITIONS *
;;;***************************
      
(deffunction MAIN::translate-av (?values)
   ;; Create the return value
   (bind ?result (create$))
   ;; Iterate over each of the allowed-values
   (progn$ (?v ?values)
      (bind ?nv
         ?v)
      ;; Add the text to the return value
      (bind ?result (create$ ?result ?nv)))
   ;; Return the return value
   ?result)

(deffunction MAIN::replace-spaces (?str)
   (bind ?len (str-length ?str))
   (bind ?i (str-index " " ?str))
   (while (neq ?i FALSE)
      (bind ?str (str-cat (sub-string 1 (- ?i 1) ?str) - (sub-string (+ ?i 1) ?len ?str)))
      (bind ?i (str-index " " ?str)))
   ?str)

(deffunction MAIN::sym-cat-multifield (?values)
   (bind ?rv (create$))
   (progn$ (?v ?values)
      (bind ?rv (create$ ?rv (sym-cat (replace-spaces ?v)))))
   ?rv)

(deffunction MAIN::multifield-to-delimited-string (?mv ?delimiter)
   (bind ?rv "")
   (bind ?first TRUE)
   (progn$ (?v ?mv)
      (if ?first
         then
         (bind ?first FALSE)
         (bind ?rv (str-cat ?v))
         else
         (bind ?rv (str-cat ?rv ?delimiter ?v))))
   ?rv)

;;;*****************
;;;* STATE METHODS *
;;;*****************

(defmethod handle-state ((?state SYMBOL (eq ?state greeting))
                         (?target SYMBOL (eq ?target gui))
                         (?message LEXEME)
                         (?relation-asserted SYMBOL)
                         (?valid-answers MULTIFIELD))
   (assert (UI-state (display ?message)
                     (relation-asserted greeting)
                     (state ?state))))

(defmethod handle-state ((?state SYMBOL (eq ?state interview))
                         (?target SYMBOL (eq ?target gui))
                         (?message MULTIFIELD)
                         (?relation-asserted SYMBOL)
                         (?response PRIMITIVE)
                         (?valid-answers MULTIFIELD)
                         (?display-answers MULTIFIELD))
   (assert (UI-state (display ?message)
                     (relation-asserted ?relation-asserted)
                     (state ?state)
                     (response ?response)
                     (valid-answers ?valid-answers)
                     (display-answers ?display-answers))))

(defmethod handle-state ((?state SYMBOL (eq ?state interview))
                         (?target SYMBOL (eq ?target gui))
                         (?message LEXEME)
                         (?relation-asserted SYMBOL)
                         (?response PRIMITIVE)
                         (?valid-answers MULTIFIELD)
                         (?display-answers MULTIFIELD))
   (assert (UI-state (display ?message)
                     (relation-asserted ?relation-asserted)
                     (state ?state)
                     (response ?response)
                     (valid-answers ?valid-answers)
                     (display-answers ?display-answers))))
 
(defmethod handle-state ((?state SYMBOL (eq ?state conclusion))
                         (?target SYMBOL (eq ?target gui))
                         (?display MULTIFIELD))
   (assert (UI-state (display ?display)
                     (state ?state)
                     (valid-answers)
                     (display-answers)))
   (assert (conclusion)))
 
(defmethod handle-state ((?state SYMBOL (eq ?state conclusion))
                         (?target SYMBOL (eq ?target gui))
                         (?display LEXEME))
   (assert (UI-state (display ?display)
                     (state ?state)
                     (valid-answers)
                     (display-answers)))
   (assert (conclusion)))

;;;****************
;;;* STARTUP RULE *
;;;****************

(defrule system-banner 
  =>
  (handle-state greeting
                ?*target*
                WelcomeMessage
                greeting
                (create$)))
  
;;;***************
;;;* QUERY RULES *
;;;***************

(defrule startup-question ""

   (greeting)
   
   =>

   (bind ?answers (create$ rock rap))
   (handle-state interview
                 ?*target*
                 StartQuestion
                 likes-genre
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))
   
(defrule suggest-radiohead-ok 

   (likes-genre rock)
   
   =>

   (bind ?answers (create$ ok cool-but-basic))
   (handle-state interview
                 ?*target*
                 radiohead-ok_computer
                 radiohead-ok-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

 
(defrule suggest-radiohead-kid-a 

   (radiohead-ok-decision cool-but-basic)

   =>
   (bind ?answers (create$ ok too-much-electronic like-bleeps-more-drums not-ready-to-lose-guitars more-electronic-with-vocals))
   (handle-state interview
                 ?*target*
                 radiohead-kid_a
                 radiohead-kid-a-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-autechre-amber 

   (or (radiohead-kid-a-decision like-bleeps-more-drums) (kashiwa_daisuke-program_music-decision more-glitchy-drums) (klaus_schulze_tangerine_dream-decision more-drums) (aphex_twin-decision cool-isnt-button-pushing))
   
   =>

   (bind ?answers (create$ ok cool-isnt-button-pushing))
   (handle-state interview
                 ?*target*
                 autechre-amber
                 autechre-amber-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-godspeed_you!_black_emperor-lift_your_skinny_fists_like_antennas_to_heaven 

   (radiohead-kid-a-decision too-much-electronic)
   
   =>

   (bind ?answers (create$ ok love-buildups i-think-i-could-handle-that-minimal i-like-post-rock-song-structure))
   (handle-state interview
                 ?*target*
                 godspeed_you!_black_emperor-lift_your_skinny_fists_like_antennas_to_heaven
                 godspeed_you!_black_emperor-lift_your_skinny_fists_like_antennas_to_heaven-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-the-knife-silent-shout 

   (radiohead-kid-a-decision more-electronic-with-vocals)
      
   =>

   (bind ?answers (create$ ok too-weird take-out-the-vocals-focus-it))
   (handle-state interview
                 ?*target*
                 the-knife-silent-shout
                 the-knife-silent-shout-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-darkside-psychic 

   (or (radiohead-kid-a-decision not-ready-to-lose-guitars) (the-knife-silent-shout-decision too-weird))   

   =>
   (bind ?answers (create$ ok like-this-lose-guitars more-the-same-im-ready where-can-i-go-from-here))
   (handle-state interview
                 ?*target*
                 darkside-psychic
                 darkside-psychic-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-aphex_twin

   (or (boards_of_canada-music_has_the_right_to_children-decision more-electric-feeling) (tokimonsta-midnight_menu-decision deal-with-weird-parts-classics) (autechre-amber-decision cool-isnt-button-pushing) )   

   =>
   (bind ?answers (create$ ok that-sounds-dated rest-of-it-no-drums steady-beat-in-that complex-beats-awesome-wilder cool-isnt-button-pushing))
   (handle-state interview
                 ?*target*
                 (translate-av (create$ aphex_twin-selected_ambient_works aphex_twin-drukqs))
                 aphex_twin-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-nicolas_jaar-space_is_only_noise 

   (darkside-psychic-decision more-the-same-im-ready)   

   =>
   (bind ?answers (create$ ok more-of-a-vocal-focus much-busy-and-melodius))
   (handle-state interview
                 ?*target*
                 nicolas_jaar-space_is_only_noise
                 nicolas_jaar-space_is_only_noise-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-bonobo-black_sands 

   (or (darkside-psychic-decision where-can-i-go-from-here) (nujabes-metaphorical_music-decision lush-organic-sounds))   

   =>
   (bind ?answers (create$ ok chill-it-out lush-organic-sounds))
   (handle-state interview
                 ?*target*
                 bonobo-black_sands
                 bonobo-black_sands-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-tycho-dive 

   (bonobo-black_sands-decision chill-it-out)   

   =>
   (bind ?answers (create$ ok make-it-stranger-educational-film))
   (handle-state interview
                 ?*target*
                 tycho-dive
                 tycho-dive-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-boards_of_canada-music_has_the_right_to_children 

   (or (tycho-dive-decision make-it-stranger-educational-film) (tokimonsta-midnight_menu-decision beats-spaced-out))   

   =>
   (bind ?answers (create$ ok more-electric-feeling))
   (handle-state interview
                 ?*target*
                 boards_of_canada-music_has_the_right_to_children
                 boards_of_canada-music_has_the_right_to_children-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-kashiwa_daisuke-program_music 

   (or (godspeed_you!_black_emperor-lift_your_skinny_fists_like_antennas_to_heaven-decision i-like-post-rock-song-structure) (venetian_snares-rossz_csillag_alatt_szuletett-decision calm-down-drums))   

   =>
   (bind ?answers (create$ ok more-ambient-part more-glitchy-drums))
   (handle-state interview
                 ?*target*
                 kashiwa_daisuke-program_music
                 kashiwa_daisuke-program_music-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-fuck_buttons-tarot_sport 

   (godspeed_you!_black_emperor-lift_your_skinny_fists_like_antennas_to_heaven-decision love-buildups)   

   =>
   (bind ?answers (create$ ok more-quiter-though))
   (handle-state interview
                 ?*target*
                 fuck_buttons-tarot_sport
                 fuck_buttons-tarot_sport-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-neu!-neu! 

   (godspeed_you!_black_emperor-lift_your_skinny_fists_like_antennas_to_heaven-decision i-think-i-could-handle-that-minimal)   

   =>
   (bind ?answers (create$ ok more-electronic-influence this-but-electronic))
   (handle-state interview
                 ?*target*
                 neu!-neu!
                 neu!-neu!-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-manuel_gottsching-e2_e4 

   (neu!-neu!-decision more-electronic-influence)   

   =>
   (bind ?answers (create$ ok fully-electronic))
   (handle-state interview
                 ?*target*
                 manuel_gottsching-e2_e4
                 manuel_gottsching-e2_e4-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-klaus_schulze_tangerine_dream 

   (or (aphex_twin-decision rest-of-it-no-drums) (kashiwa_daisuke-program_music-decision more-ambient-part) (neu!-neu!-decision this-but-electronic) (fuck_buttons-tarot_sport-decision more-quiter-though) (manuel_gottsching-e2_e4-decision fully-electronic))   

   =>
   (bind ?answers (create$ ok more-drums steady-beat-in-that shorter-songs-careless-happy more-ambient-stuff))
   (handle-state interview
                 ?*target*
                 (translate-av (create$ klaus_schulze-timewind tangerine_dream-phaedra))
                 klaus_schulze_tangerine_dream-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-kanye-west-808

   (likes-genre rap)   =>

   (bind ?answers (create$ ok give-me-some-nice-hip-hop real-rap-music))
   (handle-state interview
                 ?*target*
                 kanye_west-808s_and_heartbreak
                 kanye-west-808-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-nujabes-metaphorical_music 

   (or (kanye-west-808-decision give-me-some-nice-hip-hop) (dj_shadow-endtroducing-decision more-instrumental-beats) (bonobo-black_sands-decision lush-organic-sounds))
   
   =>

   (bind ?answers (create$ ok lush-organic-sounds stranger-wonkier))
   (handle-state interview
                 ?*target*
                 nujabes-metaphorical_music
                 nujabes-metaphorical_music-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-tokimonsta-midnight_menu 

   (or (nujabes-metaphorical_music-decision stranger-wonkier) (the-knife-silent-shout-decision take-out-the-vocals-focus-it) (burial_untrue-decision happier-lighter-focus-beats))
   
   =>

   (bind ?answers (create$ ok beats-spaced-out deal-with-weird-parts-classics even-stranger-even-wonkier keep-beat-make-dark))
   (handle-state interview
                 ?*target*
                 tokimonsta-midnight_menu
                 tokimonsta-midnight_menu-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-flying_lotus-cosmogramma 

   (or (tokimonsta-midnight_menu-decision even-stranger-even-wonkier) (aphex_twin-decision that-sounds-dated))
   
   =>

   (bind ?answers (create$ ok this-but-in-the-jungle))
   (handle-state interview
                 ?*target*
                 flying_lotus-cosmogramma
                 flying_lotus-cosmogramma-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-orb-the_orb's_adventures_beyond_ultraworld 

   (or (aphex_twin-decision steady-beat-in-that) (klaus_schulze_tangerine_dream-decision steady-beat-in-that))
   
   =>

   (bind ?answers (create$ ok little-bit-less-ambient))
   (handle-state interview
                 ?*target*
                 orb-the_orb's_adventures_beyond_ultraworld
                 orb-the_orb's_adventures_beyond_ultraworld-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-orb-the_orb's_adventures_beyond_ultraworld 

   (or (aphex_twin-decision steady-beat-in-that) (klaus_schulze_tangerine_dream-decision steady-beat-in-that))
   
   =>

   (bind ?answers (create$ ok little-bit-less-ambient))
   (handle-state interview
                 ?*target*
                 orb-the_orb's_adventures_beyond_ultraworld
                 orb-the_orb's_adventures_beyond_ultraworld-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-global_communication-76:14 

   (or (klaus_schulze_tangerine_dream-decision more-ambient-stuff) (the_future_sound_of_london-lifeforms-decision ready-for-some-ambient) (the_future_sound_of_london_orbital-decision helcyon))
   
   =>

   (bind ?answers (create$ ok even-more-ambient))
   (handle-state interview
                 ?*target*
                 global_communication-76:14
                 global_communication-76:14-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-brian_eno-ambient_2:_the_plateaux_of_mirror 

   (global_communication-76:14-decision even-more-ambient) 
   
   =>

   (bind ?answers (create$ ok album-to-busy mix-the-two))
   (handle-state interview
                 ?*target*
                 brian_eno-ambient_2:_the_plateaux_of_mirror
                 brian_eno-ambient_2:_the_plateaux_of_mirror-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-girl_talk-feed_the_animals 

   (kanye-west-808-decision real-rap-music) 
   
   =>

   (bind ?answers (create$ ok whoa-sampling-cool))
   (handle-state interview
                 ?*target*
                 girl_talk-feed_the_animals
                 girl_talk-feed_the_animals-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-burial_untrue 

   (or (tokimonsta-midnight_menu-decision keep-beat-make-dark) (the_avalanches-since_i_left_you-decision darker-sadder-samples-messed))
   
   =>

   (bind ?answers (create$ ok happier-lighter-focus-beats drop-samples-keep-beats))
   (handle-state interview
                 ?*target*
                 burial_untrue
                 burial_untrue-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-lone-galaxy_garden 

   (flying_lotus-cosmogramma-decision this-but-in-the-jungle) 
   
   =>

   (bind ?answers (create$ ok rainforest-music strange-vocals-and-drums))
   (handle-state interview
                 ?*target*
                 lone-galaxy_garden
                 lone-galaxy_garden-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-the_future_sound_of_london-lifeforms 

   (or (lone-galaxy_garden-decision rainforest-music) (the_future_sound_of_london_orbital-decision more))
   
   =>

   (bind ?answers (create$ ok ready-for-some-ambient))
   (handle-state interview
                 ?*target*
                 the_future_sound_of_london-lifeforms
                 the_future_sound_of_london-lifeforms-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-the_future_sound_of_london_orbital 

   (or (orb-the_orb's_adventures_beyond_ultraworld-decision little-bit-less-ambient) (daft_punk-discovery-decision dont-stop-dancing))
   
   =>

   (bind ?answers (create$ ok feel-on-drugs mix-focus-fast-hard-drums helcyon mix-the-two less-melody-more-subtle dont-stop-dancing))
   (handle-state interview
                 ?*target*
                 (translate-av (create$ the_future_sound_of_london-accelerator orbital-brown_album))
                 the_future_sound_of_london_orbital-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-jon_hopkins-immunity 

   (or (the_future_sound_of_london_orbital-decision mix-the-two) (brian_eno-ambient_2:_the_plateaux_of_mirror-decision mix-the-two))
      =>

   (bind ?answers (create$ ok piano-not-heavy-parts heavier-bits-collider))
   (handle-state interview
                 ?*target*
                 jon_hopkins-immunity
                 jon_hopkins-immunity-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-dj_shadow-endtroducing 

   (the_avalanches-since_i_left_you-decision more-sampling-hip-hop)   

   =>

   (bind ?answers (create$ ok more-instrumental-beats))
   (handle-state interview
                 ?*target*
                 dj_shadow-endtroducing
                 dj_shadow-endtroducing-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-the_avalanches-since_i_left_you 

   (girl_talk-feed_the_animals-decision whoa-sampling-cool)   

   =>

   (bind ?answers (create$ ok darker-sadder-samples-messed more-sampling-hip-hop))
   (handle-state interview
                 ?*target*
                 the_avalanches-since_i_left_you
                 the_avalanches-since_i_left_you-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-massive_attack-mezzanine 

   (burial_untrue-decision drop-samples-keep-beats)    

   =>

   (bind ?answers (create$ ok burial-little-better))
   (handle-state interview
                 ?*target*
                 massive_attack-mezzanine
                 massive_attack-mezzanine-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-machinedrum-rooms 

   (or (lone-galaxy_garden-decision strange-vocals-and-drums) (aphex_twin-decision complex-beats-awesome-wilder))   

   =>

   (bind ?answers (create$ ok drums-laouder-crazier))
   (handle-state interview
                 ?*target*
                 machinedrum-rooms
                 machinedrum-rooms-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-shpongle-are_you_shpongled 

   (or (the_future_sound_of_london_orbital-decision feel-on-drugs) (daft_punk-discovery-decision more-tribal))   

   =>

   (bind ?answers (create$ ok mix-focus-fast-hard-drums careless-fun-music))
   (handle-state interview
                 ?*target*
                 shpongle-are_you_shpongled
                 shpongle-are_you_shpongled-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-daft_punk-discovery 

   (or (jon_hopkins-immunity-decision heavier-bits-collider) (the_future_sound_of_london_orbital-decision dont-stop-dancing))   

   =>

   (bind ?answers (create$ ok dont-stop-dancing more-tribal dirtier careless-fun-music))
   (handle-state interview
                 ?*target*
                 daft_punk-discovery
                 daft_punk-discovery-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-venetian_snares-rossz_csillag_alatt_szuletett 

   (machinedrum-rooms-decision drums-laouder-crazier)   

   =>

   (bind ?answers (create$ ok calm-down-drums))
   (handle-state interview
                 ?*target*
                 venetian_snares-rossz_csillag_alatt_szuletett
                 venetian_snares-rossz_csillag_alatt_szuletett-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

(defrule suggest-goldie-timeless 

   (or (the_future_sound_of_london_orbital-decision mix-focus-fast-hard-drums) (shpongle-are_you_shpongled-decision mix-focus-fast-hard-drums))   

   =>

   (bind ?answers (create$ ok frozen-wasteland))
   (handle-state interview
                 ?*target*
                 goldie-timeless
                 goldie-timeless-decision
                 (nth$ 1 ?answers)
                 ?answers
                 (translate-av ?answers)))

;;;****************
;;;* RESULT RULES *
;;;****************

(defrule radiohead-ok
   (radiohead-ok-decision ok)
   =>
   (handle-state conclusion ?*target* radiohead-ok_computer))

(defrule radiohead-kid-a
   (radiohead-kid-a-decision ok)
   =>
   (handle-state conclusion ?*target* radiohead-kid_a))

(defrule autechre-amber
   (autechre-amber-decision ok)
   =>
   (handle-state conclusion ?*target* autechre-amber))

(defrule godspeed_you!_black_emperor-lift_your_skinny_fists_like_antennas_to_heaven
   (godspeed_you!_black_emperor-lift_your_skinny_fists_like_antennas_to_heaven-decision ok)
   =>
   (handle-state conclusion ?*target* godspeed_you!_black_emperor-lift_your_skinny_fists_like_antennas_to_heaven))

(defrule the-knife-silent-shout
   (the-knife-silent-shout-decision ok)
   =>
   (handle-state conclusion ?*target* the-knife-silent-shout))

(defrule darkside-psychic
   (darkside-psychic-decision ok)
   =>
   (handle-state conclusion ?*target* darkside-psychic))

(defrule aphex_twin
   (aphex_twin-decision ok)
   =>
   (handle-state conclusion ?*target* (translate-av (create$ aphex_twin-selected_ambient_works aphex_twin-drukqs))))

(defrule james_blake-james_blake
   (or (darkside-psychic-decision like-this-lose-guitars) (nicolas_jaar-space_is_only_noise-decision more-of-a-vocal-focus) (massive_attack-mezzanine-decision burial-little-better))   
   =>
   (handle-state conclusion ?*target* james_blake-james_blake))

(defrule nicolas_jaar-space_is_only_noise
   (nicolas_jaar-space_is_only_noise-decision ok)
   =>
   (handle-state conclusion ?*target* nicolas_jaar-space_is_only_noise))

(defrule villalobos-alcachofa
   (or (nicolas_jaar-space_is_only_noise-decision much-busy-and-melodius) (the_future_sound_of_london_orbital-decision less-melody-more-subtle)) 
   =>
   (handle-state conclusion ?*target* villalobos-alcachofa))

(defrule bonobo-black_sands
   (bonobo-black_sands-decision ok)
   =>
   (handle-state conclusion ?*target* bonobo-black_sands))

(defrule tycho-dive
   (tycho-dive-decision ok)
   =>
   (handle-state conclusion ?*target* tycho-dive))

(defrule boards_of_canada-music_has_the_right_to_children
   (boards_of_canada-music_has_the_right_to_children-decision ok)
   =>
   (handle-state conclusion ?*target* boards_of_canada-music_has_the_right_to_children))

(defrule kashiwa_daisuke-program_music
   (kashiwa_daisuke-program_music-decision ok)
   =>
   (handle-state conclusion ?*target* kashiwa_daisuke-program_music))

(defrule fuck_buttons-tarot_sport
   (fuck_buttons-tarot_sport-decision ok)
   =>
   (handle-state conclusion ?*target* fuck_buttons-tarot_sport))

(defrule neu!-neu!
   (neu!-neu!-decision ok)
   =>
   (handle-state conclusion ?*target* neu!-neu!))

(defrule manuel_gottsching-e2_e4
   (manuel_gottsching-e2_e4-decision ok)
   =>
   (handle-state conclusion ?*target* manuel_gottsching-e2_e4))

(defrule klaus_schulze_tangerine_dream
   (klaus_schulze_tangerine_dream-decision ok)
   =>
   (handle-state conclusion ?*target* (translate-av (create$ klaus_schulze-timewind tangerine_dream-phaedra))))

(defrule mort_garson-plantasia
   (klaus_schulze_tangerine_dream-decision shorter-songs-careless-happy)   
   =>
   (handle-state conclusion ?*target* mort_garson-plantasia))

(defrule kanye-west-808
   (kanye-west-808-decision ok)
   =>
   (handle-state conclusion ?*target* kanye_west-808s_and_heartbreak))

(defrule nujabes-metaphorical_music
   (nujabes-metaphorical_music-decision ok)
   =>
   (handle-state conclusion ?*target* nujabes-metaphorical_music))

(defrule tokimonsta-midnight_menu
   (tokimonsta-midnight_menu-decision ok)
   =>
   (handle-state conclusion ?*target* tokimonsta-midnight_menu))

(defrule flying_lotus-cosmogramma
   (flying_lotus-cosmogramma-decision ok)
   =>
   (handle-state conclusion ?*target* flying_lotus-cosmogramma))

(defrule orb-the_orb's_adventures_beyond_ultraworld
   (orb-the_orb's_adventures_beyond_ultraworld-decision ok)
   =>
   (handle-state conclusion ?*target* orb-the_orb's_adventures_beyond_ultraworld))

(defrule orb-the_orb's_adventures_beyond_ultraworld
   (orb-the_orb's_adventures_beyond_ultraworld-decision ok)
   =>
   (handle-state conclusion ?*target* orb-the_orb's_adventures_beyond_ultraworld))

(defrule global_communication-76:14
   (global_communication-76:14-decision ok)
   =>
   (handle-state conclusion ?*target* global_communication-76:14))

(defrule brian_eno-ambient_2:_the_plateaux_of_mirror
   (brian_eno-ambient_2:_the_plateaux_of_mirror-decision ok)
   =>
   (handle-state conclusion ?*target* brian_eno-ambient_2:_the_plateaux_of_mirror))

(defrule brian_eno-ambient_1:_music_for_airports
   (brian_eno-ambient_2:_the_plateaux_of_mirror-decision album-to-busy) 
   =>
   (handle-state conclusion ?*target* brian_eno-ambient_1:_music_for_airports))

(defrule girl_talk-feed_the_animals
   (girl_talk-feed_the_animals-decision ok)
   =>
   (handle-state conclusion ?*target* girl_talk-feed_the_animals))

(defrule burial_untrue
   (burial_untrue-decision ok)
   =>
   (handle-state conclusion ?*target* burial_untrue))

(defrule lone-galaxy_garden
   (lone-galaxy_garden-decision ok)
   =>
   (handle-state conclusion ?*target* lone-galaxy_garden))

(defrule the_future_sound_of_london-lifeforms
   (the_future_sound_of_london-lifeforms-decision ok)
   =>
   (handle-state conclusion ?*target* the_future_sound_of_london-lifeforms))

(defrule the_future_sound_of_london-lifeforms
   (the_future_sound_of_london-lifeforms-decision ok)
   =>
   (handle-state conclusion ?*target* the_future_sound_of_london-lifeforms))

(defrule the_future_sound_of_london_orbital
   (the_future_sound_of_london_orbital-decision ok)
   =>
   (handle-state conclusion ?*target* (translate-av (create$ the_future_sound_of_london-accelerator orbital-brown_album))))

(defrule jon_hopkins-immunity
   (jon_hopkins-immunity-decision ok)
   =>
   (handle-state conclusion ?*target* jon_hopkins-immunity))

(defrule helios-eingya
   (jon_hopkins-immunity-decision piano-not-heavy-parts)   
   =>
   (handle-state conclusion ?*target* helios-eingya))

(defrule dj_shadow-endtroducing
   (dj_shadow-endtroducing-decision ok)
   =>
   (handle-state conclusion ?*target* dj_shadow-endtroducing))

(defrule the_avalanches-since_i_left_you
   (the_avalanches-since_i_left_you-decision ok)
   =>
   (handle-state conclusion ?*target* the_avalanches-since_i_left_you))

(defrule massive_attack-mezzanine
   (massive_attack-mezzanine-decision ok)
   =>
   (handle-state conclusion ?*target* massive_attack-mezzanine))

(defrule machinedrum-rooms
   (machinedrum-rooms-decision ok)
   =>
   (handle-state conclusion ?*target* machinedrum-rooms))

(defrule shpongle-are_you_shpongled
   (shpongle-are_you_shpongled-decision ok)
   =>
   (handle-state conclusion ?*target* shpongle-are_you_shpongled))

(defrule daft_punk-discovery
   (daft_punk-discovery-decision ok)
   =>
   (handle-state conclusion ?*target* daft_punk-discovery))

(defrule venetian_snares-rossz_csillag_alatt_szuletett
   (venetian_snares-rossz_csillag_alatt_szuletett-decision ok)
   =>
   (handle-state conclusion ?*target* venetian_snares-rossz_csillag_alatt_szuletett))

(defrule goldie-timeless
   (goldie-timeless-decision ok)
   =>
   (handle-state conclusion ?*target* goldie-timeless))

(defrule sasha_and_john_digweed-northern_exposure
   (or (goldie-timeless-decision frozen-wasteland) (shpongle-are_you_shpongled-decision careless-fun-music) (daft_punk-discovery-decision careless-fun-music))
   =>
   (handle-state conclusion ?*target* sasha_and_john_digweed-northern_exposure))

(defrule justice-cross
   (daft_punk-discovery-decision dirtier) 
   =>
   (handle-state conclusion ?*target* justice-cross))

;;;*************************
;;;* GUI INTERACTION RULES *
;;;*************************

(defrule ask-question

   (declare (salience 5))
   
   (UI-state (id ?id))
   
   ?f <- (state-list (sequence $?s&:(not (member$ ?id ?s))))
             
   =>
   
   (modify ?f (current ?id)
              (sequence ?id ?s))
   
   (halt))

(defrule handle-next-no-change-none-middle-of-chain

   (declare (salience 10))
   
   ?f1 <- (next ?id)

   ?f2 <- (state-list (current ?id) (sequence $? ?nid ?id $?))
                      
   =>
      
   (retract ?f1)
   
   (modify ?f2 (current ?nid))
   
   (halt))

(defrule handle-next-response-none-end-of-chain

   (declare (salience 10))
   
   ?f <- (next ?id)

   (state-list (sequence ?id $?))
   
   (UI-state (id ?id)
             (relation-asserted ?relation))
                   
   =>
      
   (retract ?f)

   (assert (add-response ?id)))   

(defrule handle-next-no-change-middle-of-chain

   (declare (salience 10))
   
   ?f1 <- (next ?id ?response)

   ?f2 <- (state-list (current ?id) (sequence $? ?nid ?id $?))
     
   (UI-state (id ?id) (response ?response))
   
   =>
      
   (retract ?f1)
   
   (modify ?f2 (current ?nid))
   
   (halt))

(defrule handle-next-change-middle-of-chain

   (declare (salience 10))
   
   (next ?id ?response)

   ?f1 <- (state-list (current ?id) (sequence ?nid $?b ?id $?e))
     
   (UI-state (id ?id) (response ~?response))
   
   ?f2 <- (UI-state (id ?nid))
   
   =>
         
   (modify ?f1 (sequence ?b ?id ?e))
   
   (retract ?f2))
   
(defrule handle-next-response-end-of-chain

   (declare (salience 10))
   
   ?f1 <- (next ?id ?response)
   
   (state-list (sequence ?id $?))
   
   ?f2 <- (UI-state (id ?id)
                    (response ?expected)
                    (relation-asserted ?relation))
                
   =>
      
   (retract ?f1)

   (if (neq ?response ?expected)
      then
      (modify ?f2 (response ?response)))
      
   (assert (add-response ?id ?response)))   

(defrule handle-add-response

   (declare (salience 10))
   
   (logical (UI-state (id ?id)
                      (relation-asserted ?relation)))
   
   ?f1 <- (add-response ?id ?response)
                
   =>
      
   (set-fact-duplication TRUE)
   (str-assert (str-cat "(" ?relation " " ?response ")"))
   
   (retract ?f1))   

(defrule handle-add-response-none

   (declare (salience 10))
   
   (logical (UI-state (id ?id)
                      (relation-asserted ?relation)))
   
   ?f1 <- (add-response ?id)
                
   =>
      
   (set-fact-duplication TRUE)	; seems to revert back to FALSE when set once at the beginning
   (str-assert (str-cat "(" ?relation ")"))
   
   (retract ?f1))   

(defrule handle-prev

   (declare (salience 10))
      
   ?f1 <- (prev ?id)
   
   ?f2 <- (state-list (sequence $?b ?id ?p $?e))
                
   =>
   
   (retract ?f1)
   
   (modify ?f2 (current ?p))
   
   (halt))