﻿using CLIPSNET;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ElectronicMusic
{
    public partial class MainWindow : Window
    {
        private enum InterviewState { GREETING, INTERVIEW, CONCLUSION };

        private CLIPSNET.Environment clips = new CLIPSNET.Environment();
        private string lastAnswer = null;
        private string relationAsserted = null;
        private IDictionary<string, AlbumEntry> albums;
        private IDictionary<string, string> strings;

        public MainWindow()
        {
            InitializeComponent();
            prevButton.Tag = "Prev";
            clips.LoadFromString(File.ReadAllText("electronic_music.clp"));
            clips.Reset();

            albums = JsonConvert.DeserializeObject<IDictionary<string, AlbumEntry>>(File.ReadAllText("albums.json"));
            strings = JsonConvert.DeserializeObject<IDictionary<string, string>>(File.ReadAllText("strings.json"));
            prevButton.Content = "< " + GetStringByKeyOrKey("Prev");
        }

        private void HandleResponse()
        {
            var evalStr = "(find-all-facts ((?f state-list)) TRUE)";

            var currentID = (clips.Eval(evalStr) as MultifieldValue).Cast<FactAddressValue>().Last().GetFactSlot("current").ToString();
            /*===========================*/
            /* Get the current UI state. */
            /*===========================*/

            if (currentID == "nil")
                evalStr = $"(find-all-facts ((?f UI-state)) TRUE)";
            else
                evalStr = $"(find-all-facts ((?f UI-state)) (eq ?f:id {currentID}))";

            var factEvaluationResult = (MultifieldValue)clips.Eval(evalStr);

            if (factEvaluationResult.Count == 0)
            {
                MessageBox.Show("Couldn't match the rule!");

                return;
            }

            var fv = factEvaluationResult.Cast<FactAddressValue>().Last();

            /*========================================*/
            /* Determine the Next/Prev button states. */
            /*========================================*/
            
            switch (fv.GetFactSlot("state").ToString())
            {
                case "conclusion":
                    nextButton.Tag = "Restart";
                    nextButton.Content = GetStringByKeyOrKey("Restart");
                    prevButton.Visibility = Visibility.Visible;
                    choicesPanel.Visibility = Visibility.Collapsed;
                    break;
                case "greeting":
                    nextButton.Tag = "Next";
                    nextButton.Content = GetStringByKeyOrKey("Next") + " >";
                    prevButton.Visibility = Visibility.Collapsed;
                    choicesPanel.Visibility = Visibility.Collapsed;
                    break;
                default:
                    nextButton.Tag = "Next";
                    nextButton.Content = GetStringByKeyOrKey("Next") + " >";
                    prevButton.Visibility = Visibility.Visible;
                    choicesPanel.Visibility = Visibility.Visible;
                    break;
            }

            /*=====================*/
            /* Set up the choices. */
            /*=====================*/

            choicesPanel.Children.Clear();

            MultifieldValue damf = (MultifieldValue)fv.GetFactSlot("display-answers");
            MultifieldValue vamf = (MultifieldValue)fv.GetFactSlot("valid-answers");

            string selected = fv.GetFactSlot("response").ToString();
            RadioButton firstButton = null;

            for (int i = 0; i < damf.Count; i++)
            {
                LexemeValue da = (LexemeValue)damf[i];
                LexemeValue va = (LexemeValue)vamf[i];
                RadioButton rButton;
                string buttonName, buttonText, buttonAnswer;

                buttonName = GetStringByKeyOrKey(da.GetLexemeValue());
                buttonText = buttonName.Substring(0, 1).ToUpperInvariant() + buttonName.Substring(1);
                buttonAnswer = va.GetLexemeValue();

                rButton = new RadioButton
                {
                    Content = buttonText,
                    IsChecked = (lastAnswer != null && buttonAnswer.Equals(lastAnswer))
                        || (lastAnswer == null && buttonAnswer.Equals(selected)),
                    Tag = buttonAnswer,
                    Visibility = Visibility.Visible,
                    Margin = new Thickness(5)
                };

                choicesPanel.Children.Add(rButton);

                if (firstButton == null)
                { firstButton = rButton; }
            }

            if ((GetCheckedChoiceButton() == null) && (firstButton != null))
            { firstButton.IsChecked = true; }

            /*====================================*/
            /* Set the label to the display text. */
            /*====================================*/

            relationAsserted = ((LexemeValue)fv.GetFactSlot("relation-asserted")).GetLexemeValue();

            /*====================================*/
            /* Set the label to the display text. */
            /*====================================*/

            messagePanel.Children.Clear();
            coverPanel.Children.Clear();

            var messages = (MultifieldValue)fv.GetFactSlot("display");

            foreach (var message in messages)
            {
                var messageString = ((SymbolValue)message).GetLexemeValue();
                var album = GetAlbumEntryByKeyOrDefault(messageString);

                messagePanel.Children.Add(
                    new Label { Content = album?.Name ?? GetStringByKeyOrKey(messageString) });
                coverPanel.Children.Add(
                    new Image { Source = album != null && !string.IsNullOrEmpty(album.Cover) ? new BitmapImage(new Uri(album.Cover)) : null });
            }
        }

        private AlbumEntry GetAlbumEntryByKeyOrDefault(string key) => albums.ContainsKey(key) ? albums[key] : null;

        private string GetStringByKeyOrKey(string key) => strings.ContainsKey(key) ? strings[key] : key;

        private void NextButtonAction()
        {
            var evalStr = "(find-all-facts ((?f state-list)) TRUE)";

            var currentID = (clips.Eval(evalStr) as MultifieldValue).Cast<FactAddressValue>().Last().GetFactSlot("current").ToString();

            var checkedChoiceButton = GetCheckedChoiceButton();

            if (checkedChoiceButton != null)
                clips.AssertString($"(next {currentID} {GetCheckedChoiceButton().Tag})");
            else
                clips.AssertString($"(next {currentID})");
            clips.Run();

            HandleResponse();
        }

        private void PrevButtonAction()
        {
            var evalStr = "(find-all-facts ((?f state-list)) TRUE)";

            var currentID = (clips.Eval(evalStr) as MultifieldValue).Cast<FactAddressValue>().Last().GetFactSlot("current").ToString();

            clips.AssertString($"(prev {currentID})");
            clips.Run();

            HandleResponse();
        }

        private RadioButton GetCheckedChoiceButton()
            => choicesPanel.Children.Cast<RadioButton>().FirstOrDefault(c => c.IsChecked == true);

        private void OnClickButton(object sender, RoutedEventArgs e)
        {
            switch ((sender as Button).Tag)
            {
                case "Next":
                    NextButtonAction();
                    break;
                case "Restart":
                    clips.Reset();
                    clips.Run();
                    HandleResponse();
                    break;
                case "Prev":
                    PrevButtonAction();
                    break;
            }
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
            clips.Run();
            HandleResponse();
        }
    }
}
